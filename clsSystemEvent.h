//
// Created by puhovity on 12.12.15.
//

#ifndef CARS_CLSSYSTEMEVENT_H
#define CARS_CLSSYSTEMEVENT_H
#include <Box2D/Box2D.h>
#include <string>
#include <map>

class clsSystemEvent {
public:
    clsSystemEvent() {

    }
    clsSystemEvent(std::string name) {
        mName = name;
    }
    void setName(std::string name) {
        mName = name;
    }

    std::string getName() {
        return mName;
    }

    std::map<std::string, float32> mFloatParams;
private:
    std::string mName;
};

#endif //CARS_CLSSYSTEMEVENT_H
