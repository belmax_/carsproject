#ifndef CARS_CONSTANTS_H
#define CARS_CONSTANTS_H

#include <string>

const std::string SRC_PATH = "src";
const std::string IMG_PATH = SRC_PATH + "/images";
const float SCALE = 30.f;

#endif //CARS_CONSTANTS_H
