//
// Created by puhovity on 28.11.15.
//

#ifndef CARS_CLSCONTACTMANAGER_H
#define CARS_CLSCONTACTMANAGER_H
#include <string>
#include <iostream>
#include "clsCar.h"

enum ContactObjType{cotCar, cotBullet};

class clsCar;

class clsContactObj {
public:
    ContactObjType getType() {
        return mType;
    }
protected:
    ContactObjType mType;
};

class clsCarContactObj : public clsContactObj {
public:
    clsCarContactObj() : clsContactObj() {
        mType = cotCar;
        mAttachedCar = nullptr;
    }

    void setAttachedCar(clsCar* attachedCar) {
        mAttachedCar = attachedCar;
    }

    clsCar* getAttachedCar() {
        return mAttachedCar;
    }

    ~clsCarContactObj(){

    }
private:
    clsCar* mAttachedCar;
};

#endif //CARS_CLSCONTACTMANAGER_H
