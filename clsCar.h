#ifndef CARS_CLS_CAR_H
#define CARS_CLS_CAR_H

#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <utility>
#include "clsContactObj.h"
#include "clsWorldObject.h"
#include "clsWorldEvent.h"

class clsCarContactObj;
class clsWheel;
class clsRocketLauncher;

class clsCarDef : public clsWorldObjectDef {
public:
    clsCarDef() {
        setType(wot_Car);
    }

    std::pair<int, int> HPBarPos;
    std::pair<int, int> SpawnPos;

    int MaxForwardSpeed;
    int MaxBackwardSpeed;
    int MaxDriveForce;

    std::map<sf::Keyboard::Key, std::string> KeyEvents;
    sf::Texture DesignTexture;

};

class clsCar : public clsWorldObject {
public:
    clsCarDef mDef;
    b2Body *mCarBody;
    std::vector<clsWheel*> mWheels;
    std::vector<clsRocketLauncher*> mRL;

    clsCar(clsCarDef* def);
    ~clsCar();
    int getHealth();
    int setHealth(int value);
    void draw();
    void reactOnEvent(std::map<std::string, clsWorldEvent>& events);

private:
    std::map<std::string, b2RevoluteJoint*> mJoints;
    int mHealthPoints;
    clsCarContactObj* mCObj;
};



#endif //CARS_CLS_CAR_H
