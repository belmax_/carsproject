//
// Created by puhovity on 12.12.15.
//

#include "clsSystemObject.h"
#include "clsWorld.h"
#include <SFML/Graphics.hpp>
#include "clsSystemEvent.h"
#include "clsCar.h"
#include "constants.h"


void CreateBox(int MouseX, int MouseY) {
    b2BodyDef BodyDef;
    BodyDef.position = b2Vec2(MouseX / SCALE, MouseY / SCALE);
    BodyDef.type = b2_dynamicBody;
    b2Body *Body = clsWorld::getInstance()->getBox2DWorld()->CreateBody(&BodyDef);
    Body->SetUserData(nullptr);
    b2PolygonShape Shape;
    Shape.SetAsBox((32.f / 2) / SCALE, (32.f / 2) / SCALE);
    b2FixtureDef FixtureDef;
    FixtureDef.density = 1.f;
    FixtureDef.friction = 0.7f;
    FixtureDef.shape = &Shape;
    Body->CreateFixture(&FixtureDef);
}
void CreateWall(float X, float Y, float sizeX, float sizeY) {
    b2BodyDef BodyDef;
    BodyDef.position = b2Vec2(X / SCALE, Y / SCALE);
    BodyDef.type = b2_staticBody;
    b2Body *Body = clsWorld::getInstance()->getBox2DWorld()->CreateBody(&BodyDef);
    Body->SetUserData(nullptr);

    b2PolygonShape Shape;
    Shape.SetAsBox(sizeX / SCALE, sizeY / SCALE);
    b2FixtureDef FixtureDef;
    FixtureDef.density = 0.f;
    FixtureDef.shape = &Shape;
    Body->CreateFixture(&FixtureDef);
}
void DrawWall(int x1, int y1, int x2, int y2) {
    sf::Sprite WallSprite;
    WallSprite.setTexture(clsWorld::getInstance()->getTexture("wall"));
    WallSprite.setTextureRect(sf::IntRect(0, 0, x2 - x1, y2 - y1));
    WallSprite.setPosition(x1, y1);
    clsWorld::getInstance()->getRenderWindow()->draw(WallSprite);
}
void DrawGrass(int x, int y) {
    sf::Sprite GrassSprite;
    GrassSprite.setTexture(clsWorld::getInstance()->getTexture("grass"));
    GrassSprite.setTextureRect(sf::IntRect(0, 0, x, y));
    GrassSprite.setPosition(10, 14);
    clsWorld::getInstance()->getRenderWindow()->draw(GrassSprite);
}

void DrawTextures() {
    DrawGrass(1200, 800);
}

class MyContactListener : public b2ContactListener {
    void BeginContact(b2Contact* contact) {
        void* bodyAUD = contact->GetFixtureA()->GetBody()->GetUserData();
        void* bodyBUD = contact->GetFixtureB()->GetBody()->GetUserData();

        if ( bodyAUD && bodyBUD ) {
            clsContactObj* obj1 = static_cast<clsContactObj*>(bodyAUD);
            clsContactObj* obj2 = static_cast<clsContactObj*>(bodyBUD);

            if( obj1->getType() == cotCar && obj2->getType() == cotCar ) {
                clsCarContactObj* car1 = static_cast<clsCarContactObj*>(obj1);
                clsCarContactObj* car2 = static_cast<clsCarContactObj*>(obj2);

                float32 car1Damage = car2->getAttachedCar()->mCarBody->GetLinearVelocity().Length();
                float32 car2Damage = car1->getAttachedCar()->mCarBody->GetLinearVelocity().Length();

                clsWorldEvent eventCar1("car1damaged");
                eventCar1.mFloatParams["damage"] = car1Damage;

                clsWorldEvent eventCar2("car2damaged");
                eventCar2.mFloatParams["damage"] = car2Damage;

                clsWorld::getInstance()->addWorldEvent(eventCar1);
                clsWorld::getInstance()->addWorldEvent(eventCar2);
            }
        }
    }
};

clsGame::clsGame() {
    mCLListener = new MyContactListener();
    clsWorld::getInstance()->getBox2DWorld()->SetContactListener(mCLListener);

    clsCarDef Player1CarDef;
    Player1CarDef.SpawnPos = std::make_pair(300, 300);
    Player1CarDef.HPBarPos = std::make_pair(30, 30);
    Player1CarDef.KeyEvents[sf::Keyboard::W] = "forward";
    Player1CarDef.KeyEvents[sf::Keyboard::S] = "backward";
    Player1CarDef.KeyEvents[sf::Keyboard::A] = "wheelsturnleft";
    Player1CarDef.KeyEvents[sf::Keyboard::D] = "wheelsturnright";
    Player1CarDef.KeyEvents[sf::Keyboard::Q]  = "turretsturnleft";
    Player1CarDef.KeyEvents[sf::Keyboard::R]  = "turretsturnright";
    Player1CarDef.DesignTexture       = clsWorld::getInstance()->getTexture("car1");
    Player1CarDef.MaxForwardSpeed  = 100;
    Player1CarDef.MaxBackwardSpeed  = -100;
    Player1CarDef.MaxDriveForce = 50;

    clsCarDef Player2CarDef;
    Player2CarDef.SpawnPos = std::make_pair(600, 600);
    Player2CarDef.HPBarPos = std::make_pair(800, 30);
    Player2CarDef.KeyEvents[sf::Keyboard::Up] = "forward";
    Player2CarDef.KeyEvents[sf::Keyboard::Down] = "backward";
    Player2CarDef.KeyEvents[sf::Keyboard::Left] = "wheelsturnleft";
    Player2CarDef.KeyEvents[sf::Keyboard::Right] = "wheelsturnright";
    Player2CarDef.KeyEvents[sf::Keyboard::O]  = "turretsturnleft";
    Player2CarDef.KeyEvents[sf::Keyboard::P]  = "turretsturnright";
    Player2CarDef.DesignTexture= clsWorld::getInstance()->getTexture("car2");
    Player2CarDef.MaxForwardSpeed  = 300;
    Player2CarDef.MaxBackwardSpeed  = -100;
    Player2CarDef.MaxDriveForce = 50;

    clsWorld::getInstance()->createWorldObject("car1", &Player1CarDef);
    clsWorld::getInstance()->createWorldObject("car2", &Player2CarDef);

    clsWorld::getInstance()->addWorldEventSubscriber("car1");
    clsWorld::getInstance()->addWorldEventSubscriber("car2");

    // стенки
    CreateWall(0.f, 0.f, 1200, 14);
    CreateWall(0.f, 800.f, 1200, 14);
    CreateWall(0.f, 0.f, 10, 800);
    CreateWall(1200.f, 0.f, 10, 800);

}

#include <ctime>

void clsGame::draw() {

    clsWorld::getInstance()->getBox2DWorld()->Step(1/60.f, 8, 3);
    clsWorldEvent ev;
    ev.setName("car1friction");
    clsWorld::getInstance()->addWorldEvent(ev);
    ev.setName("car2friction");
    clsWorld::getInstance()->addWorldEvent(ev);
    DrawTextures();
    clsWorld::getInstance()->catchKeysPressing();

    for(std::map<std::string, clsWorldObject*>::iterator it = clsWorld::getInstance()->getWorldObjects()->begin(); it != clsWorld::getInstance()->getWorldObjects()->end(); it++ ) {
        if ( (*it).second->isSelfDrawable() ) {
            (*it).second->draw();
        }
    }


    clsWorld::getInstance()->getRenderWindow()->display();

    sf::Event event;
    if (clsWorld::getInstance()->getRenderWindow()->pollEvent(event)) {
        if (event.type == sf::Event::Closed)
            clsWorld::getInstance()->getRenderWindow()->close();
    }

}

void clsGame::reactOnEvent(clsSystemEvent &event) {
    if ( event.getName() == "escpressed" ) {
        std::cout << "esc pressed";
    }
}

clsMenu::clsMenu() {
    isEscPressed = false;
}

void clsMenu::draw() {
    clsWorld::getInstance()->getRenderWindow()->clear(sf::Color::Green);
    clsWorld::getInstance()->getRenderWindow()->display();

}

void clsMenu::reactOnEvent(clsSystemEvent &event) {
    if ( event.getName() == "escpressed" ) {
        std::cout << "esc pressed";
    }
}