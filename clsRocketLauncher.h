//
// Created by puhovity on 05.12.15.
//

#ifndef CARS_CLSROCKETLAUNCHER_H
#define CARS_CLSROCKETLAUNCHER_H

#include <SFML/Graphics/Sprite.hpp>
#include <Box2D/Box2D.h>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>

class clsCar;

struct clsRocketLauncherDef : public clsWorldObjectDef {
    clsRocketLauncherDef() {
        setType(wot_RocketLauncher);
    }
    b2Vec2 AttachedPos;
    clsCar* AttachedCar;
};

class clsRocketLauncher : public clsWorldObject {
public:
    b2Body* mRocketLauncherBody;

    clsCar* mAttachedCar;
    clsRocketLauncherDef mDef;

    clsRocketLauncher(clsRocketLauncherDef* def);
    void setAttachedCar(clsCar* attachedCar);
    void rotate(float plusAngle);
    void fire();
    void draw();

};

#endif //CARS_CLSROCKETLAUNCHER_H
