#include "clsWorld.h"
#include "constants.h"
#include "clsRocketLauncher.h"
#include "clsCar.h"
#include "clsWheel.h"
#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include "clsRocket.h"


clsRocketLauncher::clsRocketLauncher(clsRocketLauncherDef* def) {
    setName(def->mName);

    mDef = *def;
    mIsSelfDrawable = false;

    b2BodyDef bodyDef;
    bodyDef.position = (1/SCALE) * (mDef.AttachedCar->mCarBody->GetPosition() + mDef.AttachedPos);
    bodyDef.type = b2_dynamicBody;

    mRocketLauncherBody = clsWorld::getInstance()->getBox2DWorld()->CreateBody(&bodyDef);
    b2PolygonShape polygonShape;
    polygonShape.SetAsBox(5.f / SCALE, 5.f / SCALE);
    b2Fixture *fixture = mRocketLauncherBody->CreateFixture(&polygonShape, 0.1f); //shape, density
}

void clsRocketLauncher::setAttachedCar(clsCar *attachedCar) {
    mAttachedCar = attachedCar;
}

void clsRocketLauncher::fire() {
    b2Vec2 currentRightNormal = mRocketLauncherBody->GetWorldVector(b2Vec2(0, 1));
    //clsRocket rocket(mRocketLauncherBody->GetPosition(), currentRightNormal);
}

void clsRocketLauncher::rotate(float plusAngle) {
    mRocketLauncherBody->SetTransform( mRocketLauncherBody->GetPosition(), mRocketLauncherBody->GetAngle() + plusAngle);
}

void clsRocketLauncher::draw() {
    sf::Sprite sprite;
    sprite.setTexture( clsWorld::getInstance()->getTexture("turret1") );
    sprite.setOrigin(24, 24);
    b2Vec2 pos = mRocketLauncherBody->GetPosition();
    sprite.setPosition(SCALE * pos.x, SCALE * pos.y);
    sprite.setRotation(mRocketLauncherBody->GetAngle()*180/b2_pi);
    clsWorld::getInstance()->getRenderWindow()->draw(sprite);

    sf::Sprite sprite2;
    sprite2.setTexture( clsWorld::getInstance()->getTexture("lazer1") );
    sprite2.setOrigin(15, 0);
    sprite2.setPosition(SCALE * pos.x, SCALE * pos.y);
    sprite2.setRotation((mRocketLauncherBody->GetAngle())*180/b2_pi);
    clsWorld::getInstance()->getRenderWindow()->draw(sprite2);
}