#ifndef CARS_CLSWORLD_H
#define CARS_CLSWORLD_H
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include "clsWorldObject.h"
#include "clsWorldEvent.h"
#include "clsSystemObject.h"
#include <map>

// SINGLETON
class clsWorld {
public:
    static clsWorld* getInstance() {
        if ( !mInstance )
            mInstance = new clsWorld();
        return mInstance;
    }

    // get box2d and sfml render window
    b2World* getBox2DWorld();
    sf::RenderWindow* getRenderWindow();

    // default key catching
    void catchKeysPressing();

    // world objects
    std::map<std::string, clsWorldObject*>* getWorldObjects();

    clsWorldObject* createWorldObject(std::string name, clsWorldObjectDef* def);
    clsWorldObject* getWorldObject(std::string name);
    void deleteWorldObject(std::string name);

    void addWorldEventSubscriber(std::string name);
    void addWorldEvent(std::string name, clsWorldEvent& event);
    void addWorldEvent(clsWorldEvent& event);
    void sendWorldEvents();

    void addTexture(std::string name, sf::Texture& texture);
    sf::Texture& getTexture(std::string name);
    void loadTextures();

    // system
    std::map<std::string, clsSystemObject*>& getSystemObjects();
    std::map<std::string, clsSystemDrawerObject*>& getSystemDrawerObjects();
    void addSystemObject(std::string name, clsSystemObject* object);
    void addSystemDrawerObject(std::string name, clsSystemDrawerObject* object);
    clsSystemObject* getSystemObject(std::string name);
    clsSystemDrawerObject* getSystemDrawerObject(std::string name);

    void setCurrentDrawer(std::string name);
    clsSystemDrawerObject* getCurrentDrawer();

    void addSystemEventSubscriber(std::string name);
    void addSystemEvent(clsSystemEvent event);
    void sendSystemEvents();

private:
    static clsWorld* mInstance;
    clsWorld();
    clsWorld(const clsWorld&);
    clsWorld& operator=(clsWorld&);

    b2World* mBox2DWorld;
    sf::RenderWindow* mRenderWindow;

    std::map<std::string, sf::Texture> mTextures;

    std::map<std::string, clsWorldObject*> mWorldObjects;
    std::map<std::string, clsSystemObject*> mSystemObjects;
    std::map<std::string, clsSystemDrawerObject*> mSystemDrawerObjects;
    clsSystemDrawerObject* mCurrentDrawer;

    std::vector<clsWorldObject*> mWorldSubscribers;
    std::map<std::string, clsWorldEvent> mWorldEvents;
    std::vector<clsSystemEvent> mSystemEvents;
    std::vector<clsSystemObject*> mSystemSubscribers;
};
#endif //CARS_CLSWORLD_H