#include <SFML/Graphics.hpp>
#include "constants.h"

#include "clsWorld.h"
#include "clsCar.h"
#include "clsWheel.h"
#include "clsRocketLauncher.h"

clsWorld* clsWorld::mInstance = 0;

clsWorld::clsWorld() {
    mRenderWindow = new sf::RenderWindow(sf::VideoMode(1200, 800, 32), "CarFights 2D");
    mRenderWindow->setFramerateLimit(60);

    b2Vec2 Gravity(0.f, 0.f);
    mBox2DWorld = new b2World(Gravity);

    loadTextures();
}

void clsWorld::loadTextures() {
    sf::Texture WallTexture;
    WallTexture.loadFromFile(IMG_PATH + "/wall.png");
    WallTexture.setRepeated(true);
    addTexture("wall", WallTexture);

    sf::Texture WheelTexture;
    WheelTexture.loadFromFile(IMG_PATH + "/wheel.png");
    addTexture("wheel", WheelTexture);

    sf::Texture CarTexture1;
    CarTexture1.loadFromFile(IMG_PATH + "/car_01.png");
    addTexture("car1", CarTexture1);

    sf::Texture CarTexture2;
    CarTexture2.loadFromFile(IMG_PATH + "/car_02.png");
    addTexture("car2", CarTexture2);

    sf::Texture HpTexture;
    HpTexture.loadFromFile(IMG_PATH + "/hp_bar.png");
    addTexture("hp", HpTexture);

    sf::Texture GrassTexture;
    GrassTexture.loadFromFile(IMG_PATH + "/grass.jpg");
    GrassTexture.setRepeated(true);
    addTexture("grass", GrassTexture);

    sf::Texture rl1SpriteTexture;
    rl1SpriteTexture.loadFromFile(IMG_PATH + "/turret_01.png");
    addTexture("turret1", rl1SpriteTexture);

    sf::Texture lazer1SpriteTexture;
    lazer1SpriteTexture.loadFromFile(IMG_PATH + "/lazer_01.png");
    addTexture("lazer1", lazer1SpriteTexture);
}

void clsWorld::addTexture(std::string name, sf::Texture& texture) {
    mTextures.insert( std::make_pair(name, texture) );
}

b2World* clsWorld::getBox2DWorld() {
    return mBox2DWorld;
}

sf::RenderWindow* clsWorld::getRenderWindow() {
    return mRenderWindow;
}

sf::Texture& clsWorld::getTexture(std::string name) {
    return mTextures[name];
}

clsWorldObject* clsWorld::createWorldObject(std::string name, clsWorldObjectDef *def) {

    clsWorldObject* obj;

    def->mName = name;
    if ( def->getType() == wot_Car ) {
        obj = new clsCar((clsCarDef*) def);
    }
    if ( def->getType() == wot_Wheel ) {
        obj = new clsWheel((clsWheelDef*) def);
    }
    if ( def->getType() == wot_RocketLauncher ) {
        obj = new clsRocketLauncher((clsRocketLauncherDef*) def);
    }

    mWorldObjects[name] = obj;
    return obj;
}

std::map<std::string, clsWorldObject*>* clsWorld::getWorldObjects() {
    return &mWorldObjects;
}

void clsWorld::addWorldEventSubscriber(std::string name) {
    mWorldSubscribers.push_back( getWorldObject(name) );
}

void clsWorld::addWorldEvent(std::string name, clsWorldEvent& event) {
    mWorldEvents[name] = event;
}

void clsWorld::sendWorldEvents() {
    for(std::vector<clsWorldObject*>::iterator it = mWorldSubscribers.begin(); it != mWorldSubscribers.end(); it++ ) {
        (*it)->reactOnEvent(mWorldEvents);
    }
    mWorldEvents.clear();
}

clsWorldObject* clsWorld::getWorldObject(std::string name) {
    return mWorldObjects[name];
}

void clsWorld::catchKeysPressing() {

    std::map<sf::Keyboard::Key, std::string>& keysEventCar1 = ((clsCar*)clsWorld::getInstance()->getWorldObject("car1"))->mDef.KeyEvents;
    std::map<sf::Keyboard::Key, std::string>& keysEventCar2 = ((clsCar*)clsWorld::getInstance()->getWorldObject("car2"))->mDef.KeyEvents;

    for ( std::map<sf::Keyboard::Key, std::string>::iterator it1 = keysEventCar1.begin(); it1 != keysEventCar1.end(); it1++ ) {
        if (sf::Keyboard::isKeyPressed(it1->first)) {
            clsWorldEvent ev;
            ev.setName("car1"+it1->second);
            clsWorld::getInstance()->addWorldEvent(ev);
        }
    }

    for ( std::map<sf::Keyboard::Key, std::string>::iterator it2 = keysEventCar2.begin(); it2 != keysEventCar2.end(); it2++ ) {
        if (sf::Keyboard::isKeyPressed(it2->first)) {
            clsWorldEvent ev;
            ev.setName("car2"+it2->second);
            clsWorld::getInstance()->addWorldEvent(ev);
        }
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        clsSystemEvent ev;
        ev.setName("escpressed");
        mSystemEvents.push_back(ev);
    }
}

void clsWorld::addSystemObject(std::string name, clsSystemObject* object) {
    mSystemObjects[name] = object;
}

void clsWorld::addSystemDrawerObject(std::string name, clsSystemDrawerObject* object) {
    mSystemDrawerObjects[name] = object;
}

std::map<std::string, clsSystemObject*>& clsWorld::getSystemObjects() {
    return mSystemObjects;
}

std::map<std::string, clsSystemDrawerObject*>& clsWorld::getSystemDrawerObjects() {
    return mSystemDrawerObjects;
}

clsSystemObject* clsWorld::getSystemObject(std::string name) {
    return mSystemObjects[name];
}

clsSystemDrawerObject* clsWorld::getSystemDrawerObject(std::string name) {
    return mSystemDrawerObjects[name];
}

void clsWorld::deleteWorldObject(std::string name) {
    clsWorldObject* obj = getWorldObject(name);
    delete obj;
    mWorldObjects.erase(name);
}

void clsWorld::setCurrentDrawer(std::string name) {
    mCurrentDrawer = mSystemDrawerObjects[name];
}

clsSystemDrawerObject* clsWorld::getCurrentDrawer() {
    return mCurrentDrawer;
}

void clsWorld::addSystemEventSubscriber(std::string name) {
    mSystemSubscribers.push_back( getSystemObject(name) );
}

void clsWorld::addSystemEvent(clsSystemEvent event) {
    mSystemEvents.push_back(event);
}

void clsWorld::sendSystemEvents() {
    for (std::vector<clsSystemEvent>::iterator eventIt = mSystemEvents.begin(); eventIt != mSystemEvents.end(); eventIt++ ) {
        for(std::vector<clsSystemObject*>::iterator it = mSystemSubscribers.begin(); it != mSystemSubscribers.end(); it++ ) {
            (*it)->reactOnEvent(*eventIt);
        }
    }
    mSystemEvents.clear();
}

void clsWorld::addWorldEvent(clsWorldEvent &event) {
    mWorldEvents[event.getName()] = event;
}
