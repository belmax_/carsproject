//
// Created by puhovity on 05.12.15.
//

#ifndef CARS_CLSROCKET_H
#define CARS_CLSROCKET_H

#include "clsContactObj.h"
#include "clsWorldObject.h"
#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

class clsRocket : public clsWorldObject {
public:
    clsRocket(b2Vec2 spawnPos, b2Vec2 direction);
    void draw();

private:
    b2Body* mRocketBody;
};


#endif //CARS_CLSROCKET_H
