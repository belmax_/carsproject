#include "clsWorld.h"
#include "constants.h"
#include "clsCar.h"
#include "clsWheel.h"
#include "clsContactObj.h"
#include "clsRocketLauncher.h"

clsCar::clsCar(clsCarDef* def) : mDef(*def) {

    setName(def->mName);
    mType = wot_Car;
    mHealthPoints = 100;

    mIsSelfDrawable = true;
    b2BodyDef bodyDef;
    bodyDef.position = b2Vec2(mDef.SpawnPos.first / SCALE, mDef.SpawnPos.second / SCALE);
    bodyDef.type = b2_dynamicBody;

    mCarBody = clsWorld::getInstance()->getBox2DWorld()->CreateBody(&bodyDef);
    mCarBody->SetAngularDamping(3);

    mCObj = new clsCarContactObj();
    mCObj->setAttachedCar(this);
    mCarBody->SetUserData((void *) mCObj);

    b2PolygonShape polygonShape;
    polygonShape.SetAsBox(80.f / SCALE, 60.f / SCALE);
    b2Fixture *fixture = mCarBody->CreateFixture(&polygonShape, 0.1f); //shape, density

    // пушки
    clsRocketLauncherDef rlSettings;
    rlSettings.AttachedPos.x = 0;
    rlSettings.AttachedPos.y = 0;
    rlSettings.AttachedCar = this;
    clsRocketLauncher* rl1 = (clsRocketLauncher*) clsWorld::getInstance()->createWorldObject(mName+"rl_1", &rlSettings);
    mRL.push_back(rl1);

    b2RevoluteJointDef jointDef;
    jointDef.bodyA = mCarBody;
    jointDef.lowerAngle = 0;//with both these at zero...
    jointDef.upperAngle = 0;//...the joint will not move
    jointDef.localAnchorB.SetZero(); // joint anchor in tire is always center
    jointDef.bodyB = rl1->mRocketLauncherBody;
    jointDef.localAnchorA.Set(rlSettings.AttachedPos.x, rlSettings.AttachedPos.y);
    mJoints["1_rl"] = (b2RevoluteJoint *) clsWorld::getInstance()->getBox2DWorld()->CreateJoint(&jointDef);

    //b2RevoluteJointDef jointDef;
    jointDef.bodyA = mCarBody;
    jointDef.lowerAngle = 0;//with both these at zero...
    jointDef.upperAngle = 0;//...the joint will not move
    jointDef.localAnchorB.SetZero(); // joint anchor in tire is always center
    jointDef.bodyA = mCarBody;
    jointDef.lowerAngle = 0;//with both these at zero...
    jointDef.upperAngle = 0;//...the joint will not move
    jointDef.localAnchorB.SetZero(); // joint anchor in tire is always center

    //создаём 4 колеса и сохраняем их в контейнер
    clsWheelDef WheelDef;
    clsWheel* wheel;
    WheelDef.IsDrivable = true;
    WheelDef.IsRotable  = false;
    WheelDef.AttachedCar= this;

    wheel = (clsWheel*) clsWorld::getInstance()->createWorldObject(mName+"wheel_lt", &WheelDef);
    jointDef.enableLimit = true;
    jointDef.bodyB = wheel->mWheelBody;
    jointDef.localAnchorA.Set(-40.f / SCALE, 40.f / SCALE);
    mJoints["lt_wheel"] = (b2RevoluteJoint *) clsWorld::getInstance()->getBox2DWorld()->CreateJoint(&jointDef);
    mWheels.push_back(wheel);

    WheelDef.IsDrivable = true;
    WheelDef.IsRotable  = false;
    WheelDef.AttachedCar= this;
    wheel = (clsWheel*) clsWorld::getInstance()->createWorldObject(mName+"wheel_lb", &WheelDef);
    jointDef.bodyB = wheel->mWheelBody;
    jointDef.localAnchorA.Set(-40.f / SCALE, -40.f / SCALE);
    mJoints["lb_wheel"] = (b2RevoluteJoint *) clsWorld::getInstance()->getBox2DWorld()->CreateJoint(&jointDef);
    mWheels.push_back(wheel);


    WheelDef.IsDrivable = false;
    WheelDef.IsRotable  = true;
    WheelDef.AttachedCar= this;
    wheel = (clsWheel*) clsWorld::getInstance()->createWorldObject(mName+"wheel_rt", &WheelDef);
    jointDef.bodyB = wheel->mWheelBody;
    jointDef.localAnchorA.Set(40.f / SCALE, 40.f / SCALE);
    mJoints["rt_wheel"] = (b2RevoluteJoint *) clsWorld::getInstance()->getBox2DWorld()->CreateJoint(&jointDef);
    mWheels.push_back(wheel);

    WheelDef.IsDrivable = false;
    WheelDef.IsRotable  = true;
    WheelDef.AttachedCar= this;
    wheel = (clsWheel*) clsWorld::getInstance()->createWorldObject(mName+"wheel_rb", &WheelDef);
    jointDef.bodyB = wheel->mWheelBody;
    jointDef.localAnchorA.Set(40.f / SCALE, -40.f / SCALE);
    mJoints["rb_wheel"] = (b2RevoluteJoint *) clsWorld::getInstance()->getBox2DWorld()->CreateJoint(&jointDef);
    mWheels.push_back(wheel);

}

clsCar::~clsCar() {
    delete mCObj;

    clsWorld::getInstance()->deleteWorldObject(mName+"wheel_rb");
    clsWorld::getInstance()->deleteWorldObject(mName+"wheel_rt");
    clsWorld::getInstance()->deleteWorldObject(mName+"wheel_lt");
    clsWorld::getInstance()->deleteWorldObject(mName+"wheel_lb");
    //clsWorld::getInstance()->deleteWorldObject(mName+"rl_1");

    // удаляем joint's
    for( std::map<std::string, b2RevoluteJoint*>::iterator it = mJoints.begin(); it != mJoints.end(); it++ ) {
        clsWorld::getInstance()->getBox2DWorld()->DestroyJoint(it->second);
    }

    mCarBody->GetWorld()->DestroyBody(mCarBody);
}


int clsCar::getHealth() {
    return mHealthPoints;
}

int clsCar::setHealth(int value) {
    mHealthPoints = value;
}

void clsCar::draw() {

    // спрайты
    sf::Sprite CarSprite;

    CarSprite.setTexture(clsWorld::getInstance()->getTexture("car1"));

    b2Vec2 CarSpritePosition = mCarBody->GetPosition();
    CarSprite.setOrigin(81, 49);
    CarSprite.setPosition(SCALE * CarSpritePosition.x, SCALE * CarSpritePosition.y);
    CarSprite.setRotation(mCarBody->GetAngle() * 180 / b2_pi);
    clsWorld::getInstance()->getRenderWindow()->draw(CarSprite);

    sf::Sprite HP;
    HP.setTexture(clsWorld::getInstance()->getTexture("hp"));
    HP.setPosition(mDef.HPBarPos.first, mDef.HPBarPos.second);
    HP.setTextureRect(sf::IntRect(0, 0, 300 * getHealth()/100, 32));
    clsWorld::getInstance()->getRenderWindow()->draw(HP);

    for ( std::vector<clsRocketLauncher*>::iterator it = mRL.begin(); it != mRL.end(); it++ ) {
        (*it)->draw();
    }
}

void clsCar::reactOnEvent(std::map<std::string, clsWorldEvent>& events) {

    std::map<string, clsWorldEvent>::iterator eventIt;
    eventIt = events.find(getName()+"forward");
    if ( eventIt != events.end() ) {
        for( std::vector<clsWheel*>::iterator it = mWheels.begin(); it != mWheels.end(); it++ ) {
            (*it)->forward();
        }
    }

    eventIt = events.find(getName()+"backward");
    if ( eventIt != events.end() ) {
        for( std::vector<clsWheel*>::iterator it = mWheels.begin(); it != mWheels.end(); it++ ) {
            (*it)->backward();
        }
    }

    eventIt = events.find(getName()+"friction");
    if ( eventIt != events.end() ) {
        for( std::vector<clsWheel*>::iterator it = mWheels.begin(); it != mWheels.end(); it++ ) {
            (*it)->friction();
        }
    }

    eventIt = events.find(getName()+"wheelsturnleft");
    if ( eventIt != events.end() ) {
        for( std::vector<clsWheel*>::iterator it = mWheels.begin(); it != mWheels.end(); it++ ) {
            (*it)->turnLeft();
        }
    }

    eventIt = events.find(getName()+"wheelsturnright");
    if ( eventIt != events.end() ) {
        for( std::vector<clsWheel*>::iterator it = mWheels.begin(); it != mWheels.end(); it++ ) {
            (*it)->turnRight();
        }
    }

    eventIt = events.find(getName()+"damaged");
    if ( eventIt != events.end() ) {
        setHealth( getHealth() - (*eventIt).second.mFloatParams["damage"]);
    }

}