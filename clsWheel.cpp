#include "clsWorld.h"
#include "clsWheel.h"
#include "clsCar.h"

clsWheel::clsWheel(clsWheelDef* def) {

    setName(def->mName);
    mDef = *def;
    mIsSelfDrawable = true;

    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = b2Vec2(mDef.AttachedCar->mDef.SpawnPos.first / SCALE, mDef.AttachedCar->mDef.SpawnPos.second / SCALE);
    mWheelBody = clsWorld::getInstance()->getBox2DWorld()->CreateBody(&bodyDef);

    b2PolygonShape polygonShape;
    polygonShape.SetAsBox(10.f / SCALE, 10.f / SCALE);
    mWheelBody->CreateFixture(&polygonShape, 1); //shape, density
}

clsWheel::~clsWheel() {
    mWheelBody->GetWorld()->DestroyBody(mWheelBody);
}

b2Body* clsWheel::getWheelBody() {
    return mWheelBody;
}

// вектор боковой скорости
b2Vec2 clsWheel::getLateralVelocity() {
    b2Vec2 currentRightNormal = mWheelBody->GetWorldVector(b2Vec2(0, 1));
    currentRightNormal = (1 / currentRightNormal.Normalize()) * currentRightNormal;
    return b2Dot(currentRightNormal, mWheelBody->GetLinearVelocity()) * currentRightNormal;
}

// вектор текущей скорости
b2Vec2 clsWheel::getForwardVelocity() {
    b2Vec2 currentRightNormal = mWheelBody->GetWorldVector(b2Vec2(1, 0));
    return b2Dot(currentRightNormal, mWheelBody->GetLinearVelocity()) * currentRightNormal;
}

// задаём силу трения, которая каждый фрейм обновляется в зависимости от того, какая скорость
void clsWheel::friction() {
    b2Vec2 impulse = 0.6 * mWheelBody->GetMass() * (-1) * getLateralVelocity();

    mWheelBody->ApplyLinearImpulse(impulse, mWheelBody->GetWorldCenter(), 0);
    mWheelBody->ApplyAngularImpulse(12 * mWheelBody->GetInertia() * mWheelBody->GetAngularVelocity(), 0);

    b2Vec2 currentForwardNormal = getForwardVelocity();
    float currentForwardSpeed = currentForwardNormal.Normalize();
    float dragForceMagnitude = -1.5 * currentForwardSpeed;
    mWheelBody->ApplyForce(dragForceMagnitude * currentForwardNormal, mWheelBody->GetWorldCenter(), 0);
}

void clsWheel::forward() {
    if ( !mDef.IsDrivable )
        return;

    float desiredSpeed = mDef.AttachedCar->mDef.MaxForwardSpeed;

    b2Vec2 currentForwardNormal = mWheelBody->GetWorldVector(b2Vec2(1, 0));
    currentForwardNormal = (1 / currentForwardNormal.Length()) * currentForwardNormal;

    float currentSpeed = b2Dot(getForwardVelocity(), currentForwardNormal);
    float force = 0;

    // ограничиваем максимальную скорость
    if (desiredSpeed > currentSpeed)
        force = mDef.AttachedCar->mDef.MaxDriveForce;
    else if (desiredSpeed <= currentSpeed)
        return;

    mWheelBody->ApplyForceToCenter(force * currentForwardNormal, 1);
}

void clsWheel::backward() {
    if ( !mDef.IsDrivable )
        return;

    float desiredSpeed = mDef.AttachedCar->mDef.MaxBackwardSpeed;

    b2Vec2 currentForwardNormal = mWheelBody->GetWorldVector(b2Vec2(1, 0));
    currentForwardNormal = (1 / currentForwardNormal.Length()) * currentForwardNormal;

    float currentSpeed = b2Dot(getForwardVelocity(), currentForwardNormal);
    float force = 0;

    if (desiredSpeed < currentSpeed)
        force = mDef.AttachedCar->mDef.MaxDriveForce;
    else if (desiredSpeed >= currentSpeed)
        return;

    mWheelBody->ApplyForceToCenter(-force * currentForwardNormal, 1);
}

void clsWheel::turnLeft() {
    if ( !mDef.IsRotable )
        return;

    float wheelRotationAngle = -b2_pi / 4;
    if (wheelRotationAngle != 0)
        mWheelBody->SetTransform(mWheelBody->GetPosition(), mDef.AttachedCar->mCarBody->GetAngle() + wheelRotationAngle);
}

void clsWheel::turnRight() {
    if ( !mDef.IsRotable )
        return;

    float wheelRotationAngle = +b2_pi / 4;
    if (wheelRotationAngle != 0)
        mWheelBody->SetTransform(mWheelBody->GetPosition(), mDef.AttachedCar->mCarBody->GetAngle() + wheelRotationAngle);
}
void clsWheel::draw() {
    sf::Sprite WheelSprite;
    WheelSprite.setTexture( clsWorld::getInstance()->getTexture("wheel") );

    WheelSprite.setOrigin(10, 5);
    WheelSprite.setPosition(SCALE * mWheelBody->GetPosition().x,
                            SCALE * mWheelBody->GetPosition().y);
    WheelSprite.setRotation(mWheelBody->GetAngle() * 180 / b2_pi);
    clsWorld::getInstance()->getRenderWindow()->draw(WheelSprite);
}