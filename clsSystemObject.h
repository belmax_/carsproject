//
// Created by puhovity on 12.12.15.
//

#ifndef CARS_CLSSYSTEMOBJECT_H
#define CARS_CLSSYSTEMOBJECT_H
#include "clsSystemEvent.h"

class MyContactListener;

class clsSystemObject {
public:
    virtual void reactOnEvent(clsSystemEvent& event) {};
};

class clsSystemDrawerObject : public clsSystemObject {
public:
    virtual void draw() {};
};

class clsMenu : public clsSystemDrawerObject {
public:
    clsMenu();
    void draw();
    void reactOnEvent(clsSystemEvent& event);
private:
    bool isEscPressed;
};

class clsGame : public clsSystemDrawerObject {
public:
    clsGame();
    void draw();
    void reactOnEvent(clsSystemEvent& event);
private:
    MyContactListener* mCLListener;
};

#endif //CARS_CLSSYSTEMOBJECT_H
