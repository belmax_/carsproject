#ifndef CARS_CLS_WHEEL_H
#define CARS_CLS_WHEEL_H
#include <Box2D/Box2D.h>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Texture.hpp>
#include "constants.h"

class clsCar;

struct clsWheelDef : public clsWorldObjectDef {
    clsWheelDef() {
        setType(wot_Wheel);
    }

    bool IsRotable;
    bool IsDrivable;
    clsCar* AttachedCar;
};

class clsWheel : public clsWorldObject {
public:
    clsWheelDef mDef;

    b2Body* mWheelBody;
    clsWheel(clsWheelDef* def);
    ~clsWheel();
    b2Body* getWheelBody();

    // вектор боковой скорости
    b2Vec2 getLateralVelocity();
    // вектор текущей скорости
    b2Vec2 getForwardVelocity();

    void forward();
    void backward();
    void turnLeft();
    void turnRight();
    // задаём силу трения, которая каждый фрейм обновляется в зависимости от того, какая скорость
    void friction();
    void draw();
};

#endif //CARS_CLS_WHEEL_H
