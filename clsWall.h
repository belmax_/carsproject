//
// Created by puhovity on 06.12.15.
//

#ifndef CARS_CLSWALL_H
#define CARS_CLSWALL_H

#include "clsWorld.h"
#include "clsWorldObject.h"

class clsWall : public clsWorldObject {
public:
    void draw();
};


#endif //CARS_CLSWALL_H
