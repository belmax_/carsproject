#include <SFML/Graphics.hpp>
#include <Box2D/Box2D.h>
#include <string>
#include <vector>
#include <iostream>

#include "constants.h"
#include "clsWorld.h"
#include "clsWorldObject.h"
#include "clsWorldEvent.h"

#include "clsCar.h"
#include "clsWheel.h"
#include "clsContactObj.h"
#include "clsRocketLauncher.h"
#include "clsSystemObject.h"

int main() {
    clsGame* game = new clsGame();
    clsMenu* menu = new clsMenu();

    clsWorld::getInstance()->addSystemDrawerObject("game", game);
    clsWorld::getInstance()->addSystemDrawerObject("menu", menu);

    clsWorld::getInstance()->addSystemEventSubscriber("game");
    clsWorld::getInstance()->addSystemEventSubscriber("menu");

    clsWorld::getInstance()->setCurrentDrawer("game");
    while( clsWorld::getInstance()->getRenderWindow()->isOpen() ) {
        clsWorld::getInstance()->getCurrentDrawer()->draw();
        clsWorld::getInstance()->sendSystemEvents();
        clsWorld::getInstance()->sendWorldEvents();
    }

    delete game;
    delete menu;
}