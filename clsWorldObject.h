#ifndef CARS_CLSWORLDOBJECT_H
#define CARS_CLSWORLDOBJECT_H
#include <string>
#include "clsWorldEvent.h"
#include <iostream>

using std::string;

enum WorldObjectType {wot_Car, wot_Wheel, wot_RocketLauncher, wot_Bullet};

class clsWorldObjectDef {
public:
    std::string mName;
    void setType(WorldObjectType type) {
        mType = type;
    }

    WorldObjectType getType() {
        return mType;
    }

private:
    WorldObjectType mType;

};

class clsWorldObject {
protected:
    bool mIsSelfDrawable; // отрисовывается ли само, либо вызывается из других отрисовщиков
    string mName;
    WorldObjectType mType;

public:
    virtual void draw() {};
    virtual void reactOnEvent(std::map<std::string, clsWorldEvent>& events) {

    };
    virtual ~clsWorldObject() {

    }

    bool isSelfDrawable() {
        return mIsSelfDrawable;
    }

    string getName() {
        return mName;
    }

    void setName(std::string name) {
        mName = name;
    }

    void setType(WorldObjectType type) {
        mType = type;
    }

    WorldObjectType getType() {
        return mType;
    }
};


#endif //CARS_CLSWORLDOBJECT_H
