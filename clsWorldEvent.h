#ifndef CARS_CLSWORLDEVENT_H
#define CARS_CLSWORLDEVENT_H

#include <string>
#include <Box2D/Box2D.h>
#include <map>

class clsWorldEvent {
public:
    clsWorldEvent() {

    }
    clsWorldEvent(std::string name) {
        mName = name;
    }
    void setName(std::string name) {
        mName = name;
    }

    std::string getName() {
        return mName;
    }

    std::map<std::string, float32> mFloatParams;
private:
    std::string mName;
};


#endif //CARS_CLSWORLDEVENT_H
